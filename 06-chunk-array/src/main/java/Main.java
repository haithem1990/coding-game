import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("############# try ############");
            System.out.println("enter the tab size");
            int n = scanner.nextInt();
            Integer[] tab = new Integer[n];
            for (int i = 0; i < n; i++) {
                System.out.println("enter " + i + " :");
                tab[i] = scanner.nextInt();
            }
            System.out.println("enter the chunk :");
            int chunkNbr = scanner.nextInt();
            chunk(tab, chunkNbr);
        }
    }
    public static void chunk(Integer[] tab, int chunkNbr) {
        for (int i = 0; i < tab.length; i++) {
            if (i%chunkNbr==0) {
                System.out.print("\n");
            }
            System.out.print(tab[i]);
        }
        System.out.println("\n ############## End ###############");
    }
}
