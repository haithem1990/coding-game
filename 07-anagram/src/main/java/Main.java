import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("############# try ############");
            System.out.println("enter the first str01 :");
            String str01 = scanner.nextLine();
            System.out.println("enter the second str02 :");
            String str02 = scanner.nextLine();
            anagram(str01, str02);
        }
    }

    // abbc ===> nbcabst ====> true
    // ghhf ====> llml =====> false
    public static void anagram(String str01, String str02) {

        Map<String, Integer> map01 = buildOccurrenceMap(str01);
        Map<String, Integer> map02 = buildOccurrenceMap(str02);
        Boolean isAnagram = Boolean.TRUE;
        for(Map.Entry<String,Integer> mapEntry : map01.entrySet()){
            if(mapEntry.getValue()!=map02.get(mapEntry.getKey())){
                isAnagram = Boolean.FALSE;
                break;
            }
        }
        System.out.println("str01 = " + str01 + ", str02 = " + str02 + ",=====> " + isAnagram);
    }

    public static Map<String,Integer> buildOccurrenceMap(String str){
        Map<String, Integer> map = new LinkedHashMap<>();
        for (int i = 0; i < str.length(); i++) {
            if(Character.isAlphabetic(str.charAt(i))){
            String characterAsStr = String.valueOf(str.charAt(i)).toLowerCase();
            Integer occurrence = (map.get(characterAsStr) != null ? map.get(characterAsStr) : 0);
            map.put(characterAsStr, occurrence + 1);
        }
        }
        return map;
    }
}
