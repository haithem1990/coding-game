import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("############# try ############");
            System.out.println("enter :");
            int n = scanner.nextInt();
            int[][] matrix = spiralMatrix(n);
            for (int i=0;i< n;i++){
                for (int j=0;j< n;j++){
                    System.out.printf(String.valueOf(matrix[i][j]));
                }
                System.out.println("\n");
            }
        }
    }

    public static int[][] spiralMatrix(int n) {
       int[][] matrix = new int[n][n];

       int startRow = 0;
       int endRow = n-1;
       int startColumn=0;
       int endColumn=n-1;
       int counter=1;
       while(startRow<=endRow && startColumn<=endColumn){
           for (int i=startColumn;i<=endColumn;i++){
           matrix[startRow][i] = counter;
           ++counter;
       }
           ++startRow;
           for (int i=startRow;i<=endRow;i++){
               matrix[i][endColumn] = counter;
               ++counter;
           }
          --endColumn;
           for (int i=endColumn;i>=startColumn;i--){
               matrix[endRow][i] = counter;
               ++counter;
           }
           --endRow;
           for (int i=endRow;i>=startRow;i--){
               matrix[i][startColumn] = counter;
               ++counter;
           }
           ++startColumn;
       }
       return matrix;
    }
}