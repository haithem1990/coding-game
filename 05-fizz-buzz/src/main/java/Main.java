import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        while (true){
            int nbr = scanner.nextInt();
            fizzBuzz(nbr);
        }
    }
    public static void fizzBuzz(int nbr){
        for(int i=1;i<=nbr;i++){
            if(i%3==0 && i%5==0){
                System.out.print("{fizzbuzz} ");
            }
            else if(i%3==0 && i%5!=0){
                System.out.print("{fizz} ");
            }
            else if(i%3!=0 && i%5==0){
                System.out.print("{buzz} ");
            } else {
                System.out.print("{"+i+"} ");
            }
        }
    }
}
