import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String entryStr = scanner.nextLine();
            maxChar(entryStr);
        }
    }

    public static void maxChar(String str) {
        if (str == null || str.isEmpty()) {
            System.out.println("empty entry refused");
        }else {
        Map<String, Integer> map = new LinkedHashMap<>();
        map.put(String.valueOf(str.charAt(0)), 1);
        for (int i = 1; i < str.length(); i++) {
            Integer occurrence = map.get(String.valueOf(str.charAt(i)));
            if (occurrence == null) {
                map.put(String.valueOf(str.charAt(i)), 1);
            } else {
                map.put(String.valueOf(str.charAt(i)),occurrence+1);
            }
        }
        int maxOccurrence = 0;
        String letter = "";
        for (Map.Entry<String, Integer> item : map.entrySet()) {
            if (item.getValue() > maxOccurrence) {
                maxOccurrence = item.getValue();
                letter = item.getKey();
            }
        }
        System.out.println(letter + " === max occurrence ==> " + maxOccurrence);
    }
    }
}
