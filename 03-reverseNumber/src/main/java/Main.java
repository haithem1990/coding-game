import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            try {
                System.out.println("######### try ##########");
                Scanner scanner = new Scanner(System.in);
                Integer entryInt = scanner.nextInt();
                reverseNumber(entryInt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void reverseNumber(Integer nbr) {
        if (nbr == null) {
            System.out.println("null entry not supported");
        } else if (nbr == 0) {
            System.out.println("0 ===> 0");
        } else {
            String nbrAsString = String.valueOf((nbr > 0 ? nbr : nbr * (-1)));
            char[] chars = nbrAsString.toCharArray();

            Boolean countRightZero = Boolean.TRUE;
            int rightZeroCount = 0;
            int index = nbrAsString.length() - 1;
            while (countRightZero && index >= 0) {
                if (!String.valueOf(chars[index]).equals("0")) {
                    countRightZero = Boolean.FALSE;
                } else {
                    ++rightZeroCount;
                }
                --index;
            }
            String formatStrWithoutRightZero = nbrAsString.substring(0, nbrAsString.length() - rightZeroCount);
            String reversedStr = "";
            for (int i = formatStrWithoutRightZero.length() - 1; i >= 0; i--) {
                reversedStr += String.valueOf(formatStrWithoutRightZero.charAt(i));
            }
            Integer reversed = Integer.valueOf(reversedStr);

            System.out.println(nbr + " ====> " + (nbr > 0 ? reversed : reversed * (-1)));
        }
    }
}
