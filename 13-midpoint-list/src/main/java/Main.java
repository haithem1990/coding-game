import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.start();
    }

    public void start() {
        customList list = new customList();
        for (int i=0; i<6; i++)
        {
            list.push(i);
        }
        list.printList();
        list.printMiddle();
    }


    class customList {
        Node head;

        public void push(int data)
        {
            Node node = new Node(data);
            node.next = head;
            head = node;
        }

        public void printList()
        {
            Node node = head;
            while (node != null)
            {
                System.out.print(node.data+"->");
                node = node.next;
            }
            System.out.println("NULL");
        }

        void printMiddle()
        {
            Node slow = head;
            Node fast = head;
            if (head != null)
            {
                while (fast != null && fast.next != null)
                {
                    fast = fast.next.next;
                    slow = slow.next;
                }
                System.out.println("The middle element is [" +
                        slow.data + "] \n");
            }
        }

        class Node {

            Integer data;
            Node next;

            public Node(Integer data) {
                this.data = data;
            }

            public Node(Integer data, Node next) {
                this.data = data;
                this.next = next;
            }
        }
    }
}