import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("############# try ############");
            System.out.println("enter :");
            int entry = scanner.nextInt();
            print(entry);
        }
    }
    public static void print(int n){
        int m = n*2-1;
        for (int i=1;i<=n;i++){
            for (int j=1;j<=m;j++){
                if (j <= n+i-1 && j>=n-i+1) {
                    System.out.print("*");
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
            }
    }
    }