import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("############# try ############");
            System.out.println("enter :");
            String entry = scanner.nextLine();
            extractVowels(entry);
        }
    }

    public static void extractVowels(String str) {
        int vowelsCount = 0;
        for (int i = 0; i < str.length(); i++) {
            Character c = Character.toLowerCase(str.charAt(i));
            if (c.equals('a') ||c.equals('e') || c.equals('i') || c.equals('o') || c.equals('u')) {
                ++vowelsCount;
            }
        }
        System.out.println(vowelsCount);
    }
}