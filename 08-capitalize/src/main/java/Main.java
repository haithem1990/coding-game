import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("############# try ############");
            System.out.println("enter :");
            String entry = scanner.nextLine();
            capitalize(entry);
        }
    }

    public static void capitalize(String str){
        String[] splitStr = str.split(" ");
        for (int i=0;i<splitStr.length;i++){
            char firstChar = splitStr[i].charAt(0);
            splitStr[i]=splitStr[i].replaceFirst(String.valueOf(firstChar),String.valueOf(firstChar).toUpperCase());
        }
        String result = Arrays.stream(splitStr).collect(Collectors.joining(" "));
        System.out.println("str = " + str + " ==========> " + result);
    }
}
