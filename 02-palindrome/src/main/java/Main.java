import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String entryStr = scanner.nextLine();
            System.out.println(isPalindrome(entryStr));
        }
    }

    public static Boolean isPalindrome(String str) {
        if (str == null || str.isEmpty()) {
            return Boolean.FALSE;
        }
        Boolean isPalindrome = Boolean.TRUE;
        int middleIndex = str.length() / 2;
        char[] chars = str.toCharArray();
        int index = 0;
        while (isPalindrome && index <= middleIndex) {
            if (chars[index] != chars[chars.length - index - 1]) {
                isPalindrome = Boolean.FALSE;
            }
            index++;
        }
        return isPalindrome;
    }
}
